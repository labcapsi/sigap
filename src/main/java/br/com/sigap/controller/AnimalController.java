package br.com.sigap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.sigap.dao.AnimalDAO;
import br.com.sigap.model.Animal;

@Controller
@Transactional
public class AnimalController {

	@Autowired 
	private AnimalDAO animalDAO;

	@Secured("ROLE_ADMIN")
	@RequestMapping("/animal/cadastrar")
	public String cadastrar(Model model) {
		return "/animal/cadastrar";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping("/animal/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		Animal animal = animalDAO.selecionar(id);
		model.addAttribute("animal", animal);
		return "/animal/editar";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping("/animal/apagar/{id}")
	public String apagar(@PathVariable Long id, Model model) {
		Animal animal = animalDAO.selecionar(id);
		animalDAO.remover(animal);
		return "redirect:/animal/listar";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping("/animal/salvar")
	public String salvar(Animal animal) {
		animalDAO.salvar(animal);
		return "redirect:/animal/listar";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping("/animal/atualizar")
	public String atualizar(Animal animal) {
		animalDAO.atualizar(animal);
		return "redirect:/animal/listar";
	}
	
	@RequestMapping("/animal/listar")
	public String listar(Model model) {
		List<Animal> animais = animalDAO.listar();
		model.addAttribute("animais", animais);
		return "/animal/listar";
	}

}
