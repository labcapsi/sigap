<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Editar Informações</h1>
<form class="form-horizontal" action="/curso-mvc/pasto/atualizar"
	method="post">

	<input type="hidden" name="id" value="${pasto.id}">

	<div class="form-group">
		<label for="pastagem" class="col-sm-2 control-label">Pastagem (Verde/Seca):</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="pastagem" value="${pasto.pastagem}">
		</div>
	</div>

	<div class="form-group">
		<label for="pastagem_tipo" class="col-sm-2 control-label">Tipo de Pastagem:</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="pastagem_tipo"
				value="${pasto.pastagem_tipo}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Atualizar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>