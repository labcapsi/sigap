package br.com.sigap.model;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import java.util.Calendar;
import java.util.List;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.Hours;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.sigap.model.Animal;

public class ProdLactacao {

	@OneToOne
	private Animal codAnimal;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar Data;

	@DateTimeFormat(pattern = "HH:mm")
	@Temporal(TemporalType.TIME)
	private Hours Hora;

	@OneToMany(mappedBy = "lactacao")
	private List<MaoDeObra> maoDeObras;

	@NotBlank
	private float qtdLactacao;

	private Usuario user;

	public Animal getCodAnimal() {
		return codAnimal;
	}

	public void setCodAnimal(Animal codAnimal) {
		this.codAnimal = codAnimal;
	}

	public Calendar getData() {
		return Data;
	}

	public void setData(Calendar data) {
		Data = data;
	}

	public Hours getHora() {
		return Hora;
	}

	public void setHora(Hours hora) {
		Hora = hora;
	}

	public List<MaoDeObra> getMaoDeObras() {
		return maoDeObras;
	}

	public void setMaoDeObras(List<MaoDeObra> maoDeObras) {
		this.maoDeObras = maoDeObras;
	}

	public float getQtdLactacao() {
		return qtdLactacao;
	}

	public void setQtdLactacao(float qtdLactacao) {
		this.qtdLactacao = qtdLactacao;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

}
