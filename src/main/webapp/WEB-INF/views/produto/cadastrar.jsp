<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Registrar produto</h1>
<form class="form-horizontal" action="/curso-mvc/produto/salvar"
	method="post">

	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Nome do produto</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="nome">
		</div>
	</div>

	<div class="form-group">
		<label for="quantidade" class="col-sm-2 control-label">Quantidade</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="quantidade">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Salvar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>