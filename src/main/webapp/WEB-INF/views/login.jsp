<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>


<jsp:include
	page="/WEB-INF/views/template_login/template_login.jsp"></jsp:include>

<div class="body"></div>
<div class="grad"></div>
<div class="header">
<!--  Logotipo e T�tulo -->
<a class="navbar-brand" href="#"> <img alt="LOGO_SIGAP"
	src="http://s28.postimg.org/463a6jhpl/logo_sigap_2.png">
</a>
	<p>
	<div>
		Sistema Integrado<br>
		<span>de Gerenciamento Agropecu�rio</span>
	</div>
	</p>
</div>
<br>

<div class="login">
	<c:if test="${param.error!=null }">
		<div class="alert alert-danger">Usu�rio ou senha inv�lidos</div>
	</c:if>
	<c:if test="${param.logout!=null }">
		<div class="alert alert-info">Logout efetuado com sucesso</div>
	</c:if>
	<c:url value="/login" var="login"></c:url>
	<springForm:form action="${login }" cssClass="form-horizontal"
		method="post">

		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">Usu�rio</label>
			<div class="col-sm-10">
				<input type="text" name="username" id="username"
					class="form-control" />
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Senha</label>
			<div class="col-sm-10">
				<input type="password" name="password" id="password"
					class="form-control" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Entrar</button>
			</div>
		</div>
	</springForm:form>
</div>

<script
	src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

</div>
</div>
</div>
</body>
</html>