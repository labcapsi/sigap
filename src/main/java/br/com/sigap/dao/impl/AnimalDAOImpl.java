package br.com.sigap.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sigap.dao.AnimalDAO;
import br.com.sigap.model.Animal;

@Repository
public class AnimalDAOImpl extends GenericDAOImpl<Long, Animal> implements AnimalDAO {

	public AnimalDAOImpl() {
		super(Animal.class);
	}

	@PersistenceContext
	private EntityManager entityManager;

}