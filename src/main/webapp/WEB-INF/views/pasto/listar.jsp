<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Pastos</h1>
<table class="table">
	<tr>
		<th>Pastagem</th>
		<th>Tipo de Pastagem</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
	<c:forEach items="${pastos}" var="pasto">
		<tr>
			<td>${pasto.pastagem}</td>
			<td>${pasto.pastagem_tipo}</td>
			<td><a href="/curso-mvc/pasto/editar/${pasto.id}">Editar</a></td>
			<td><a href="/curso-mvc/pasto/apagar/${pasto.id}">Apagar</a></td>
		</tr>
	</c:forEach>
</table>

<br />
<a class="btn btn-primary" href="/curso-mvc/pasto/cadastrar">Cadastrar Outros Pastos</a>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>