package br.com.sigap.model;

import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import java.util.Calendar;
import java.util.List;
import org.joda.time.Hours;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.sigap.model.Animal;

public class ProdReproducao {

	private Animal codMatriz;

	@OneToMany(mappedBy = "filho")
	private List<Animal> codFilho;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar dataParto;

	@DateTimeFormat(pattern = "HH:mm")
	@Temporal(TemporalType.TIME)
	private Hours horaParto;

	private Usuario user;

	@OneToMany(mappedBy = "maodeobra")
	private List<MaoDeObra> funcionarios;

	public Animal getCodMatriz() {
		return codMatriz;
	}

	public void setCodMatriz(Animal codMatriz) {
		this.codMatriz = codMatriz;
	}

	public List<Animal> getCodFilho() {
		return codFilho;
	}

	public void setCodFilho(List<Animal> codFilho) {
		this.codFilho = codFilho;
	}

	public Calendar getDataParto() {
		return dataParto;
	}

	public void setDataParto(Calendar dataParto) {
		this.dataParto = dataParto;
	}

	public Hours getHoraParto() {
		return horaParto;
	}

	public void setHoraParto(Hours horaParto) {
		this.horaParto = horaParto;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public List<MaoDeObra> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<MaoDeObra> funcionarios) {
		this.funcionarios = funcionarios;
	}

}
