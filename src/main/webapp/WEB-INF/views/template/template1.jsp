<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<!-- <link rel="stylesheet" href="http://getbootstrap.com/examples/carousel/carousel.css"> -->

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style type="text/css">
@media ( min-width : 768px) {
	.container {
		max-width: 1024px;
	}
}

.container-narrow>hr {
	margin: 30px 0;
}

.dropdown-submenu {
	position: relative;
}

.dropdown-submenu>.dropdown-menu {
	top: 0;
	left: 100%;
	margin-top: -6px;
	margin-left: -1px;
	-webkit-border-radius: 0 6px 6px 6px;
	-moz-border-radius: 0 6px 6px;
	border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
	display: block;
}

.dropdown-submenu>a:after {
	display: block;
	content: " ";
	float: right;
	width: 0;
	height: 0;
	border-color: transparent;
	border-style: solid;
	border-width: 5px 0 5px 5px;
	border-left-color: #ccc;
	margin-top: 5px;
	margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
	border-left-color: #fff;
}

.dropdown-submenu.pull-left {
	float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
	left: -100%;
	margin-left: 10px;
	-webkit-border-radius: 6px 0 6px 6px;
	-moz-border-radius: 6px 0 6px 6px;
	border-radius: 6px 0 6px 6px;
}

.navbar-text {
	margin: 5px;
	text-align: center;
	width: 200px;
}

.navbar-brand {
	padding: 0px;
}

.navbar-brand>img {
	height: 50px;
	padding: 0px;
	/* 	width: 100px; */
}

.body{
	position: absolute;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background-image: url(http://images.123hdwallpapers.com/20150513/farm-wallpaper-1920x1080.jpg);
	background-size: cover;
/* 	-webkit-filter: blur(5px); */
	z-index: 0;
	opacity: 0.1;
}
</style>

</head>
<body>
<div class="body"></div>
	<div class="navbar-wrapper">
		<div class="container">

			<nav class="navbar navbar-inverse navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#navbar"
							aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>

						<!--  Logotipo e T�tulo -->
						<a class="navbar-brand" href="#"> <img alt="SIGAP"
							src="http://s28.postimg.org/463a6jhpl/logo_sigap_2.png">
						</a>
						<p class="navbar-text">
							Sistema Integrado de <br> Gerenciamento Agropecu�rio
						</p>

					</div>

					<div id="navbar" class="navbar-collapse collapse">

						<ul class="nav navbar-nav">
							<li class="active"><a href="#">Home</a></li>
							<li><a href="#sobre">Sobre o Projeto</a></li>
							<li><a href="#contatos">Contatos</a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-haspopup="true"
								aria-expanded="false">Menu<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li class="dropdown-submenu"><a href="#">Pesquisar</a>
										<ul class="dropdown-menu">
											<li><a href="/curso-mvc/fazenda/listar">Fazendas</a></li>
											<li><a href="/curso-mvc/funcionario/listar">Funcion�rios</a></li>
											<li><a href="/curso-mvc/produto/listar">Produtos</a></li>
											<li><a href="/curso-mvc/animal/listar">Animais</a></li>
											<li><a href="/curso-mvc/lote/listar">Lotes</a></li>
											<li><a href="/curso-mvc/pasto/listar">Pastos</a></li>
										</ul></li>

									<li class="dropdown-submenu"><a href="#">Cadastrar</a>
										<ul class="dropdown-menu">
											<li class="disabled"><a
												href="/curso-mvc/fazenda/cadastrar">Fazendas</a></li>
											<li class="dropdown-submenu"><a href="#">M�o de Obra</a>
												<ul class="dropdown-menu">
													<li class="disabled"><a href="#">Veterinario</a></li>
													<li class="disabled"><a href="#">Estagi�rio</a></li>
													<li class="disabled"><a href="#">Vaqueiro</a></li>
												</ul></li></li>
									<li><a href="/curso-mvc/produto/cadastrar">Produtos</a></li>
									<li><a href="/curso-mvc/animal/cadastrar">Animais</a></li>
									<li><a href="/curso-mvc/lote/cadastrar">Lotes</a></li>
									<li><a href="/curso-mvc/pasto/cadastrar">Pastos</a></li>
								</ul></li>

							<li role="separator" class="divider"></li>
							<li class="disabled"><a href="#">Relat�rios</a></li>
						</ul>

						<security:authorize access="hasRole('ADMIN')">
							<li>Administrador</li>
						</security:authorize>
						<security:authorize access="isAuthenticated()">

							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-haspopup="true"
								aria-expanded="false"> <security:authentication
										property="principal.username" /> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><c:url value="/logout" var="logout"></c:url> <springForm:form
											action="${logout }">
												<button type="submit" class="btn btn-link">Efetuar Logoff</button>
										</springForm:form></li>
								</ul></li>
						</security:authorize>

					</div>
				</div>
			</nav>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">