package br.com.sigap.dao;

import br.com.sigap.model.Usuario;

public interface UsuarioDAO extends GenericDAO<String, Usuario> {

	Usuario buscarPorEmail(String email);

}
