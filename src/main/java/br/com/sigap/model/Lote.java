package br.com.sigap.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import org.hibernate.validator.constraints.NotBlank;
import br.com.sigap.model.Animal;

@Entity
public class Lote {

	@Id
	@GeneratedValue
	private long codLote;

	@NotBlank
	private Long area;

	@OneToMany(mappedBy = "lote")
	private List<Animal> animals;

	@OneToMany(mappedBy = "lote")
	private List<MaoDeObra> maoDeObras;

	public long getCodLote() {
		return codLote;
	}

	public void setCodLote(long codLote) {
		this.codLote = codLote;
	}

	public Long getArea() {
		return area;
	}

	public void setArea(Long area) {
		this.area = area;
	}

	public List<Animal> getAnimals() {
		return animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}

	public List<MaoDeObra> getMaoDeObras() {
		return maoDeObras;
	}

	public void setMaoDeObras(List<MaoDeObra> maoDeObras) {
		this.maoDeObras = maoDeObras;
	}

}
