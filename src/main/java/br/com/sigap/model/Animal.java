package br.com.sigap.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
//import javax.persistence.OneToMany;
//import java.util.List;
import javax.persistence.ManyToOne;
import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Animal {

	@Id
	@GeneratedValue
	private Long codAnimal;

	@NotBlank
	private String nome;

	//vou deixar comentada aqui...ok
	/*@NotBlank
	private Animal animal;*/

	@NotBlank
	private int idade;

	@NotBlank
	private char sexo;

	@NotBlank
	private String identificacao;

	/* Inicio Enumarations */
	enum Especie {
		ESPECIE1, ESPECIE2
	}

	@Enumerated(EnumType.STRING)
	Especie especie;

	enum Categoria {
		CAT1, CAT2
	}

	@Enumerated(EnumType.STRING)
	Categoria categoria;

	enum Raca {
		RACA1, RACA2
	}
	
	@Enumerated(EnumType.STRING)
	Raca raca;

	enum Finalidade {
		FIN1, FIN2
	}

	@Enumerated(EnumType.STRING)
	Finalidade finalidade;

	/* Fim Enumarations */
	
	@ManyToOne
	@JoinColumn(name = "Animal")
	private ProdCorte prodCorte;
	
	@ManyToOne
	@JoinColumn(name = "lote_id")
	private Lote lote;
	
	
	public Long getCodAnimal() {
		return codAnimal;
	}

	public void setCodAdnimal(Long codAnimal) {
		this.codAnimal = codAnimal;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	/*public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}*/

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public Lote getLote() {
		return lote;
	}

	public void setLote(Lote lote) {
		this.lote = lote;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Raca getRaca() {
		return raca;
	}

	public void setRaca(Raca raca) {
		this.raca = raca;
	}

	public Finalidade getFinalidade() {
		return finalidade;
	}

	public void setFinalidade(Finalidade finalidade) {
		this.finalidade = finalidade;
	}

	public ProdCorte getProdCorte() {
		return prodCorte;
	}

	public void setProdCorte(ProdCorte prodCorte) {
		this.prodCorte = prodCorte;
	}

	public void setCodAnimal(Long codAnimal) {
		this.codAnimal = codAnimal;
	}

}
