package br.com.sigap.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Insumo {
	
	@Id
	@GeneratedValue
	private Long codInsumo;
	
	enum tipoInsumo {
		Adubo, Sementes, Concentrado, MatLimpeza, MatVeterinario
	}

	enum tipoUso {
		Maquinario, Herbicida, Vestuario
	}

	tipoInsumo tipoinsumos;

	tipoUso tipousos;

	@ManyToOne
	@JoinColumn(name = "insumo")
	private ProdRacao prodRacao;
	
	
	public tipoInsumo getTipoinsumos() {
		return tipoinsumos;
	}

	public void setTipoinsumos(tipoInsumo tipoinsumos) {
		this.tipoinsumos = tipoinsumos;
	}

	public tipoUso getTipousos() {
		return tipousos;
	}

	public void setTipousos(tipoUso tipousos) {
		this.tipousos = tipousos;
	}
	
	

}
