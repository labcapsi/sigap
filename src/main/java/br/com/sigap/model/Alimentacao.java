package br.com.sigap.model;

import java.util.Calendar;

import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class Alimentacao {

	@OneToOne
	private ProdMassaVerde massaVerde;

	@OneToOne
	private ProdRacao racao;

	private float qtdRacao;

	private float qtdMassaVerde;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar data;

	public ProdMassaVerde getMassaVerde() {
		return massaVerde;
	}

	public void setMassaVerde(ProdMassaVerde massaVerde) {
		this.massaVerde = massaVerde;
	}

	public ProdRacao getRacao() {
		return racao;
	}

	public void setRacao(ProdRacao racao) {
		this.racao = racao;
	}

	public float getQtdRacao() {
		return qtdRacao;
	}

	public void setQtdRacao(float qtdRacao) {
		this.qtdRacao = qtdRacao;
	}

	public float getQtdMassaVerde() {
		return qtdMassaVerde;
	}

	public void setQtdMassaVerde(float qtdMassaVerde) {
		this.qtdMassaVerde = qtdMassaVerde;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

}
