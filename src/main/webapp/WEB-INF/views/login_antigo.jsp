<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>


<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>


<div class="panel panel-default">
	<div class="panel-heading">
		<h1>�rea Restrita:</h1>
	</div>
	<div class="panel-body">
		<c:if test="${param.error!=null }">
			<div class="alert alert-danger">Usu�rio ou senha inv�lidos</div>
		</c:if>
		<c:if test="${param.logout!=null }">
			<div class="alert alert-info">Logout efetuado com sucesso</div>
		</c:if>
		<c:url value="/login" var="login"></c:url>
		<springForm:form action="${login }" cssClass="form-horizontal"
			method="post">

			<div class="form-group">
				<label for="username" class="col-sm-2 control-label">Usu�rio</label>
				<div class="col-sm-10">
					<input type="text" name="username" id="username"
						class="form-control" />
				</div>
			</div>

			<div class="form-group">
				<label for="password" class="col-sm-2 control-label">Senha</label>
				<div class="col-sm-10">
					<input type="password" name="password" id="password"
						class="form-control" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Salvar</button>
				</div>
			</div>
		</springForm:form>
	</div>

</div>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>