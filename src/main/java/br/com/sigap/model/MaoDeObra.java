package br.com.sigap.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class MaoDeObra {

	@Id
	@GeneratedValue
	private int codCadastro;

	@NotBlank
	private String nome;

	@NotBlank
	private int idade;

	@NotBlank
	private float salario;

	enum Turno{
		Manh�, Tarde, Noite
	}
	
	Turno turno;
	
	@ManyToOne
	@JoinColumn(name = "lote_id")
	private Lote lote;

	@ManyToOne
	@JoinColumn(name = "corte_id")
	private ProdCorte prodCorte ;	
	
	@ManyToOne
	@JoinColumn(name = "racao_id")
	private ProdRacao prodRacao ;
	
	public int getCodCadastro() {
		return codCadastro;
	}

	public void setCodCadastro(int codCadastro) {
		this.codCadastro = codCadastro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}

	public Lote getLote() {
		return lote;
	}

	public void setLote(Lote lote) {
		this.lote = lote;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public ProdCorte getProdCorte() {
		return prodCorte;
	}

	public void setProdCorte(ProdCorte prodCorte) {
		this.prodCorte = prodCorte;
	}

}