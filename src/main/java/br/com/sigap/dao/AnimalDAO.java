package br.com.sigap.dao;

import br.com.sigap.model.Animal;

public interface AnimalDAO extends GenericDAO<Long, Animal>{

}