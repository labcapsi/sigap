package br.com.sigap.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Calendar;
import java.util.List;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.sigap.model.Animal;
import br.com.sigap.model.MaoDeObra;

@Entity
public class ProdCorte {

	@Id
	@GeneratedValue
	private Long codCorte;

	@OneToMany(mappedBy = "prodCorte")
	private List<Animal> animals;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar Data;

	@DateTimeFormat(pattern = "HH:mm")
	@Temporal(TemporalType.TIME)
	private Date Hora;

	@OneToMany(mappedBy = "prodCorte")
	private List<MaoDeObra> maoDeObras;

	@ManyToOne
	@JoinColumn(name = "ProdCorte")
	private Animal animal;

	private float qtdCorte;

	private Usuario user;

	public Long getCodCorte() {
		return codCorte;
	}

	public void setCodCorte(Long codCorte) {
		this.codCorte = codCorte;
	}

	public List<Animal> getAnimals() {
		return animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}

	public Calendar getData() {
		return Data;
	}

	public void setData(Calendar data) {
		Data = data;
	}

	public Date getHora() {
		return Hora;
	}

	public void setHora(Date hora) {
		Hora = hora;
	}

	public List<MaoDeObra> getMaoDeObras() {
		return maoDeObras;
	}

	public void setMaoDeObras(List<MaoDeObra> maoDeObras) {
		this.maoDeObras = maoDeObras;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public float getQtdCorte() {
		return qtdCorte;
	}

	public void setQtdCorte(float qtdCorte) {
		this.qtdCorte = qtdCorte;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

}
