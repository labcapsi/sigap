package br.com.sigap.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ProdRacao {

	@Id
	@GeneratedValue
	private Long codRacao;

	@OneToMany(mappedBy = "prodRacao")
	private List<Insumo> insumos;

	@OneToMany(mappedBy = "prodRacao")
	private List<MaoDeObra> maoDeObras;

	public Long getCodRacao() {
		return codRacao;
	}

	public void setCodRacao(Long codRacao) {
		this.codRacao = codRacao;
	}

	public List<Insumo> getInsumos() {
		return insumos;
	}

	public void setInsumos(List<Insumo> insumos) {
		this.insumos = insumos;
	}

	public List<MaoDeObra> getMaoDeObras() {
		return maoDeObras;
	}

	public void setMaoDeObras(List<MaoDeObra> maoDeObras) {
		this.maoDeObras = maoDeObras;
	}

}
