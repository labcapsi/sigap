<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Registrar Pastos</h1>
<form class="form-horizontal" action="/curso-mvc/pasto/salvar"
	method="post">

	<div class="form-group">
		<label for="pastagem" class="col-sm-2 control-label">Pastagem:</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="pastagem" placeholder="Verde ou Seca...">
		</div>
	</div>

	<div class="form-group">
		<label for="pastagem_tipo" class="col-sm-2 control-label">Tipo de Pastagem:</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="pastagem_tipo" placeholder="Natural / Nativa / Cultivada...">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Salvar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>