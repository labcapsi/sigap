package br.com.sigap.model;

import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import java.util.Calendar;
import java.util.List;
import org.springframework.format.annotation.DateTimeFormat;

public class ProdMassaVerde {

	private int codProdMassaVerde;

	@OneToMany(mappedBy = "maodeobra")
	private List<MaoDeObra> funcionario;

	@OneToMany(mappedBy = "insumo")
	private List<Insumo> insumos;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar periodoInicio;

	public int getCodProdMassaVerde() {
		return codProdMassaVerde;
	}

	public void setCodProdMassaVerde(int codProdMassaVerde) {
		this.codProdMassaVerde = codProdMassaVerde;
	}

	public List<MaoDeObra> getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(List<MaoDeObra> funcionario) {
		this.funcionario = funcionario;
	}

	public List<Insumo> getInsumos() {
		return insumos;
	}

	public void setInsumos(List<Insumo> insumos) {
		this.insumos = insumos;
	}

	public Calendar getPeriodoInicio() {
		return periodoInicio;
	}

	public void setPeriodoInicio(Calendar periodoInicio) {
		this.periodoInicio = periodoInicio;
	}

}
